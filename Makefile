# Makefile

EXES += gobackup.exe

all:: $(EXES)

gofmt:
	gofmt -w gobackup.go

clean::
	-/bin/rm -vf *.exe
	-/bin/rm -vf $(EXES)

%.exe: %.go; go build -o $@ $<

install::
	/bin/cp gobackup.timer /etc/systemd/system/
	/bin/cp gobackup.service /etc/systemd/system/
	/bin/chmod 664 /etc/systemd/system/gobackup.*
	systemctl daemon-reload
	systemctl enable gobackup.timer
	systemctl start  gobackup.timer

#end
