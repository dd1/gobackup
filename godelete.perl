#!/usr/bin/perl -w
#
# zfs list -H -p -t all > zfs.list
#

my $dry_run = grep {/\-n/} @ARGV;

my %fs;
my %date;
my %used;
my %refer;

my $df = `df -kl /pool/gobackup | grep pool`;

my ($fs, $size, $used, $available) = split(/\s+/, $df);

print "[$df] [$size] [$used] [$available]\n";

my $free_space = $available*1000;

#my $zfs_list = `cat zfs.list`;
print "Reading zfs list...\n";
my $zfs_list = `zfs list -H -p -t all | grep ^pool/gobackup`;
print "Reading zfs list... done\n";

foreach my $in (split(/\n/, $zfs_list)) {
    #chop $in;
    my ($name, $used, $avail, $refer, $mountpoint) = split(/\s+/, $in);
    #print "line [$in] [$name] [$used] [$avail] [$refer] [$mountpoint]\n";
    my ($fs, $date) = split(/\@/, $name);
    next unless $date;
    #print "[$name] [$date] [$used] [$avail] [$refer] [$mountpoint]\n";

    $fs{$name} = $fs;
    $date{$name} = $date;
    $used{$name} = 1.0*$used;
    $refer{$name} = 1.0*$refer;
}

my $total_used = 0;

my $top_used = 10;

print "Top disk used:\n";

my %fs_used;
my %fs_max_used;

foreach my $k (sort {$used{$b} <=> $used{$a}} keys %date)
{
    my $name = $k;
    my $date = $date{$k};
    my $used = $used{$k};
    my $refer = $refer{$k};
    my $fs    = $fs{$k};

    my $x = toGiB($used);

    $total_used += $used;

    $fs_used{$fs} += $used;

    if (!$fs_max_used{$fs} || ($used > $fs_max_used{$fs})) {
	$fs_max_used{$fs} = $used;
    }
    
    if ($top_used > 0) {
	$top_used--;
	print "[$date] [$x] [$name] [$used] [$refer]\n";
    }
}

my $xtotal_used = toGiB($total_used);

print "Total used: $xtotal_used\n";

my $max_used_sum = 0;

print "Used by fs:\n";
foreach my $fs (sort {$fs_used{$b} <=> $fs_used{$a}} keys %fs_used)
{
    my $fs_used = $fs_used{$fs};
    my $fs_max_used = $fs_max_used{$fs};

    $max_used_sum += $fs_max_used;

    my $xfs_used = toGiB($fs_used);
    my $xfs_max_used = toGiB($fs_max_used);
    print "[$xfs_used] [$xfs_max_used] [$fs]\n";
}

my $wanted_free_space = 2.0*$max_used_sum;
my $to_delete = $wanted_free_space - $free_space;

my $xwanted_free_space = toGiB($wanted_free_space);
my $xmax_used_sum = toGiB($max_used_sum);
my $xfree_space = toGiB($free_space);
my $xto_delete  = toGiB($to_delete);

print "Sum of biggest snapshots: [$xmax_used_sum]\n";
print "Wanted free space: [$xwanted_free_space]\n";
print "Current free space: [$xfree_space]\n";
print "To be deleted: [$xto_delete]\n";

my $cumul_used = 0;

my @cmd;

foreach my $k (sort {$date{$a} cmp $date{$b}} keys %date)
{
    my $name = $k;
    my $date = $date{$k};
    my $used = $used{$k};
    my $refer = $refer{$k};

    $cumul_used += $used;

    if ($cumul_used < $to_delete) {
      push @cmd, "zfs destroy $k";
    }

    my $pct = $cumul_used/$total_used;

    my $xused = toGiB($used);
    my $xcumul_used = toGiB($cumul_used);
    my $xpct = sprintf("%.2f%%", $pct*100.0);

    print "[$date] [$xused] [$xcumul_used] [$xtotal_used] [$xpct] [$name] [$used] [$refer]\n";
}

my $xcumul_used = toGiB($cumul_used);

print "Total used accounted $xcumul_used out of $xtotal_used\n";

my $count = 100;

print "Delete commands:\n";
foreach my $c (@cmd)
{
    print "$c\n";
    system $c unless $dry_run;
    last if ($count < 1);
    $count--;
}

exit 0;

sub toGiB
{
    my $sz = shift @_;
    return sprintf("%.3f GiB", $sz/(1024.0*1024.0*1024.0));
}

# end
