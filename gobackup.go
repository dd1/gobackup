//
// gobackup.go
//

package main

import "flag"
import "fmt"
import "io"
import "os"
import "os/exec"
import "time"
import "strings"
import "log"
import "io/ioutil"
import "regexp"
import "bytes"
import "strconv"
import "path/filepath"
import "sort"
import "net/smtp"

const KiB = 1024
const MiB = 1024 * 1024
const GiB = 1024 * 1024 * 1024

// debug flags

var verbose = flag.Bool("v", false, "report progress activity")
var dryrun = flag.Bool("n", false, "dry run - report all activity but do not do anything")

// location of config file

var config_file = flag.String("c", "gobackup.conf", "configuration file")

// overrides settings in config file

var backup_root_flag = flag.String("C", "", "ZFS mount point to hold the backup tree, overrides config file backup_root")
var zfs_root_flag = flag.String("Z", "", "ZFS pool to hold the backup tree, overrides config file zfs_root")
var report_root_flag = flag.String("S", "", "path where reports are saved, overrides config file report_root")
var report_rsync_flag = flag.String("s", "", "rsync backup report to given destination, i.e. -s ladd00:~daqweb/public_html/daqbackup, overrides config file report_rsync")
var report_email_flag = flag.String("e", "", "email report to given address, i.e. -e root, overrides config file report_email")

// main actions

var delete_flag = flag.Bool("D", false, "delete old backups before running new backups")
var backup_flag = flag.Bool("B", false, "run a backup cycle")
var time_flag = flag.Int("t", 20, "backup cycle duration in hours")

// maintenance actions

var init_flag = flag.Bool("i", false, "initialize backup directories")
var add_flag = flag.Bool("a", false, "add new backup element")
var cleanup_flag = flag.Bool("K", false, "cleanup backup element")

// reporting actions

var list_flag = flag.Bool("l", false, "list backup elements")
var report_flag = flag.Bool("R", false, "generate report")
var html_flag = flag.Bool("H", false, "generate html report")

func main() {
	// Parse command line

	flag.Parse()

	config_file := *config_file
	config := read_config(config_file)

	backup_root := config["backup_root"]
	zfs_root := config["zfs_root"]
	report_root := config["report_root"]
	report_rsync := config["report_rsync"]
	report_email := config["report_email"]

	if len(*backup_root_flag) > 0 {
		backup_root = *backup_root_flag
	}

	if len(*zfs_root_flag) > 0 {
		zfs_root = *zfs_root_flag
	}

	if len(*report_root_flag) > 0 {
		report_root = *report_root_flag
	}

	if len(*report_rsync_flag) > 0 {
		report_rsync = *report_rsync_flag
	}

	if len(*report_email_flag) > 0 {
		report_email = *report_email_flag
	}

	if *verbose {
		log.Printf("backup_root: \"%s\"\n", backup_root)
		log.Printf("zfs_root:    \"%s\"\n", zfs_root)
		log.Printf("report_root:  \"%s\"\n", report_root)
		log.Printf("report_rsync: \"%s\"\n", report_rsync)
		log.Printf("report_email: \"%s\"\n", report_email)
	}

	if len(backup_root) < 1 {
		log.Fatalf("Error: backup_root is blank")
	}

	if len(zfs_root) < 1 {
		log.Fatalf("Error: zfs_root is blank")
	}

	if len(report_root) < 1 {
		log.Fatalf("Error: report_root is blank")
	}

	// Initialize directories, ZFS filesystems

	if *init_flag {

		if !file_exists(backup_root) {
			log.Printf("mkdir \"%s\"\n", backup_root)

			err := os.Mkdir(backup_root, 0700)
			if err != nil {
				fmt.Fprintf(os.Stderr, "os.Mkdir(%s) error %v\n", backup_root, err)
				return
			}
		}

		if !file_exists(report_root) {
			log.Printf("mkdir \"%s\"\n", report_root)

			err := os.Mkdir(report_root, 0700)
			if err != nil {
				fmt.Fprintf(os.Stderr, "os.Mkdir(%s) error %v\n", report_root, err)
				return
			}
		}

		zfs_create_command := "zfs create " + zfs_root

		log.Printf("create ZFS filesystem: %s\n", zfs_create_command)

		c := exec.Command("/bin/sh", "-c", zfs_create_command)
		out, err := c.CombinedOutput()
		if err != nil {
			log.Printf("ZFS create failed, error %v\n", err)
			log.Printf("ZFS create output: %s\n", chop(string(out)))
		}

		// set ZFS options

		zfs_set_command := "zfs set snapdir=visible " + zfs_root

		log.Printf("set ZFS options: %s\n", zfs_set_command)

		c = exec.Command("/bin/sh", "-c", zfs_set_command)
		out, err = c.CombinedOutput()
		if err != nil {
			log.Printf("ZFS set failed, error %v\n", err)
			log.Printf("ZFS set output: %s\n", chop(string(out)))
		}

		return
	}

	// List backup elements

	if *list_flag {
		be_list := list_backup_elements(backup_root)

		for _, be := range be_list {
			host := read_host(backup_root, be)
			if len(host) < 1 {
				continue
			}
			path := read_path(backup_root, be)
			if len(path) < 1 {
				continue
			}
			age := get_age(backup_root, be)
			report := get_lastreport(backup_root, be)
			fmt.Printf("%s: %s:%s age %.1f sec last %s\n", be, host, path, age, report)
			//fmt.Println(be)
		}
		return
	}

	// Generate report for all backup elements

	if *report_flag && len(flag.Args()) == 0 {
		report_all(backup_root, report_root, report_rsync)
		return
	}

	// Cleanup all backup elements

	if *cleanup_flag && len(flag.Args()) == 0 {
		be_list := list_backup_elements(backup_root)

		for _, be := range be_list {
			host := read_host(backup_root, be)
			if len(host) < 1 {
				continue
			}
			path := read_path(backup_root, be)
			if len(path) < 1 {
				continue
			}

			cleanup(backup_root, zfs_root, be)
		}

		return
	}

	// Backup all backup elements

	if *backup_flag && len(flag.Args()) == 0 {

		delete_report := ""

		// Delete old backups
		if *delete_flag {
			delete_report = delete(zfs_root)
		}

		be_list := list_backup_elements(backup_root)

		var m map[string]float64
		m = make(map[string]float64)

		var list []string

		for _, be := range be_list {
			host := read_host(backup_root, be)
			if len(host) < 1 {
				continue
			}
			path := read_path(backup_root, be)
			if len(path) < 1 {
				continue
			}

			list = append(list, be)
			m[be] = get_age(backup_root, be)
		}

		// sort by age, oldest first

		sort.Slice(list, func(i, j int) bool { return m[list[i]] >= m[list[j]] })

		t0 := time.Now()
		ts := t0.Format("20060102-15:04:05")

		log.Printf("Starting backup cycle, %d backup elements, %d hours\n", len(list), *time_flag)

		reports := ""
		errors := ""
		count_errors := 0

		count := 0
		for _, be := range list {
			count++
			log.Printf("Backup %d out of %d: %s\n", count, len(list), be)

			report := backup(backup_root, zfs_root, be)

			reports += fmt.Sprintf("%-30s %s\n", be, report)

			if strings.HasPrefix(report, "Error") {
				errors += fmt.Sprintf("%-30s %s\n", be, report)
				count_errors++
			}

			elapsed := time.Now().Sub(t0).Seconds()
			if elapsed > float64(60*60*(*time_flag)) {
				log.Printf("Stopping backup cycle after %.1f hours out of %d permitted\n", elapsed/(60*60), *time_flag)
				break
			}
		}

		elapsed := time.Now().Sub(t0).Seconds()

		log.Printf("Backup cycle finished in %.1f hours, %d out of %d elements, %d errors\n", elapsed/(60*60), count, len(list), count_errors)

		report_all(backup_root, report_root, report_rsync)

		email := ""
		email += "\n"
		email += fmt.Sprintf("gobackup report %s\n", ts)
		email += "\n"

		if len(delete_report) > 0 {
			email += delete_report
			email += "\n"
			email += "\n"
		}

		if count_errors > 0 {
			email += "Errors:\n"
			email += "\n"
			email += errors
			email += "\n"
		}

		email += "All backups elements:\n"
		email += "\n"
		email += reports

		email += "\n"
		email += fmt.Sprintf("Backup cycle finished in %.1f hours, %d out of %d elements, %d errors\n", elapsed/(60*60), count, len(list), count_errors)

		email += "\n"
		email += "--\n"
		email += "gobackup\n"

		if len(report_email) > 0 {
			subject := fmt.Sprintf("gobackup report %s", ts)
			if count_errors > 0 {
				subject += fmt.Sprintf(", %d errors", count_errors)
			}
			send_email(report_email, subject, email)
		}

		return
	}

	// Delete old backups
	if *delete_flag {
		delete(zfs_root)
		return
	}

	// Generate html report for all backup elements

	if *html_flag {
		log.Fatalf("Generation of html report temporary turned off\n")
		return

		log_dir := backup_root + "/*/logs/report.txt"

		if *verbose {
			log.Printf("Log dir: %s\n", log_dir)
		}

		logs, err := filepath.Glob(log_dir)
		if err != nil {
			log.Fatalf("Cannot list report files, error %v\n", err)
		}

		html_th := ""
		html_th += "<tr>\n"
		html_th += "<th>Date</th>\n"
		html_th += "<th>Element</th>\n"
		html_th += "<th>Len</th>\n"
		html_th += "<th>Lines</th>\n"
		html_th += "<th>Files total</th>\n"
		html_th += "<th>Files xfer</th>\n"
		html_th += "<th>Files del</th>\n"
		html_th += "<th>MiB total</th>\n"
		html_th += "<th>MiB xfer</th>\n"
		html_th += "<th>MiB lit</th>\n"
		html_th += "<th>List time</th>\n"
		html_th += "<th>Elapsed time</th>\n"
		html_th += "</tr>\n"

		html_all := ""

		html_last := make(map[string]string)
		html_be := make(map[string]string)

		for _, log_file := range logs {
			if *verbose {
				fmt.Println(log_file)
			}

			bes := strings.Split(log_file, "/")
			be := bes[len(bes)-3]

			data, err := ioutil.ReadFile(log_file)

			if err != nil {
				log.Printf("Cannot read report file, error %v\n", err)
				continue
			}

			lines := bytes.Split(data, []byte("\n"))

			for _, line := range lines {
				x := strings.Fields(string(line))
				html_tr := ""
				last := false
				if len(x) == 10 {

					html_tr += "<tr>"

					html_tr += "<td align=left>"
					html_tr += strings.TrimSuffix(x[0], ".log")
					html_tr += "</td>\n"

					last = strings.HasPrefix(x[0], "last")

					html_tr += "<td align=left>"
					html_tr += be
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[1]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[2]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[3]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[4]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[5]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[6]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[7]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[8]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[9]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += "</td>\n"

					//html_tr += "<td align=left>"
					//html_tr += string(line);
					//html_tr += "</td>\n";

					html_tr += "</tr>\n"
				} else if len(x) == 11 {

					html_tr += "<tr>"

					html_tr += "<td align=left>"
					html_tr += strings.TrimSuffix(x[0], ".log")
					html_tr += "</td>\n"

					last = strings.HasPrefix(x[0], "last")

					html_tr += "<td align=left>"
					html_tr += be
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[1]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[2]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[3]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[4]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[5]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[6]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[7]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[8]
					html_tr += "</td>\n"

					html_tr += "<td align=right>"
					html_tr += x[9]
					html_tr += "</td>\n"

					// doctor the elapsed string to make it sortable in a useful way
					// 4:46:31 - is 4 hours
					// 45:28.71 - 45 min, make it 0:45:28, drop sub-seconds

					elapsed := x[10]

					i := strings.Index(elapsed, ".")
					if i > 0 {
						elapsed = "0:" + elapsed[:i]
					}

					html_tr += "<td align=right>"
					html_tr += elapsed
					html_tr += "</td>\n"

					//html_tr += "<td align=left>"
					//html_tr += string(line);
					//html_tr += "</td>\n";

					html_tr += "</tr>\n"
				}
				if len(html_tr) > 0 {
					html_all += html_tr
					if !last {
						html_last[be] = html_tr
					}
					html_be[be] += html_tr
				}
			}
		}

		html_table_last := ""
		keys_last := make([]string, 0, len(html_last))
		for key := range html_last {
			keys_last = append(keys_last, key)
		}
		sort.Strings(keys_last)
		for _, k := range keys_last {
			html_table_last += html_last[k]
		}

		html := ""
		html += "<!DOCTYPE html>\n"
		html += "<html>\n"
		html += "<head>\n"
		html += "<script type=\"text/javascript\" src=\"sorttable.js\"></script>\n"
		html += "<title>gobackup report</title>\n"
		html += "</head>\n"
		html += "<body>\n"
		html += "<h1>gobackup report last backup</h1>\n"
		html += "<table border=1 align=\"center\" class=\"sortable\">\n"
		html += html_th
		html += html_table_last
		html += "</table>\n"
		html += "<h1>gobackup report everything</h1>\n"
		html += "<table border=1 align=\"center\" class=\"sortable\">\n"
		html += html_th
		html += html_all
		html += "</table>\n"
		html += "</body>\n"
		html += "</html>\n"

		html_file := report_root + "/report.html"

		err = ioutil.WriteFile(html_file, []byte(html), 0444)

		if err != nil {
			log.Fatalf("Cannot write html report file, error %v\n", err)
		}

		log.Printf("Wrote html report file: %s\n", html_file)

		return
	}

	// Identify backup element

	args := flag.Args()
	if len(args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage: gobackup hostname fs ### backup one element\n")
		fmt.Fprintf(os.Stderr, "Usage: gobackup -D -B -t 20 ### run a backup cycle\n")
		fmt.Fprintf(os.Stderr, "Usage: gobackup -i ### initialize the zfs pool\n")
		fmt.Fprintf(os.Stderr, "Usage: gobackup -a hostname fs ### add a new backup element\n")
		return
	}

	host := args[0]
	fs := args[1]

	fs_tag := make_fstag(fs)

	be := host + "_" + fs_tag

	// Add new backup element

	if *add_flag {
		zfs_target_path := zfs_root + "/" + be
		target_path := backup_root + "/" + be

		err := os.Mkdir(target_path, 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "os.Mkdir(%s) error %v\n", target_path, err)
			return
		}

		zfs_create_command := "zfs create " + zfs_target_path

		if *verbose {
			log.Printf("ZFS create command: %s\n", zfs_create_command)
		}

		c := exec.Command("/bin/sh", "-c", zfs_create_command)
		zcout, err := c.CombinedOutput()
		if err != nil {
			log.Printf("ZFS create failed, error %v\n", err)
			log.Printf("ZFS create output: %s\n", chop(string(zcout)))
			return
		}

		err = os.Mkdir(target_path+"/data", 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "os.Mkdir() error %v\n", err)
		}
		err = os.Mkdir(target_path+"/logs", 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "os.Mkdir() error %v\n", err)
		}
		err = os.Mkdir(target_path+"/conf", 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "os.Mkdir() error %v\n", err)
		}
		err = ioutil.WriteFile(exclude_file(backup_root, be), nil, 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ioutil.WriteFile() error %v\n", err)
		}
		maybe_write_host(backup_root, be, host)
		maybe_write_path(backup_root, be, fs)
		return
	}

	maybe_write_host(backup_root, be, host)
	maybe_write_path(backup_root, be, fs)

	// Cleanup backup element

	if *cleanup_flag {
		cleanup(backup_root, zfs_root, be)
		return
	}

	// Generate report for backup element

	if *report_flag {
		generate_last_report(backup_root, be)
		//log_dir := target_path + "/logs"
		//generate_report(log_dir)
		return
	}

	backup(backup_root, zfs_root, be)
}

//
// generate path names for config and log files
//

func host_file(backup_root, be string) string {
	return backup_root + "/" + be + "/conf/host.txt"
}

func path_file(backup_root, be string) string {
	return backup_root + "/" + be + "/conf/path.txt"
}

func exclude_file(backup_root, be string) string {
	return backup_root + "/" + be + "/conf/exclude.txt"
}

func newlog_file(backup_root, be string) string {
	return backup_root + "/" + be + "/logs/new.log"
}

func lastlog_file(backup_root, be string) string {
	return backup_root + "/" + be + "/logs/last.log"
}

func lastreport_file(backup_root, be string) string {
	return backup_root + "/" + be + "/logs/last_report.txt"
}

//
// read and write config files
//

func read_host(backup_root, be string) string {
	file := host_file(backup_root, be)
	data, err := ioutil.ReadFile(file)
	if err != nil {
		//log.Printf("read_host: ioutil.ReadFile(%s) error %v\n", file, err)
		return ""
	}
	return chop(string(data))
}

func read_path(backup_root, be string) string {
	file := path_file(backup_root, be)
	data, err := ioutil.ReadFile(file)
	if err != nil {
		//log.Printf("read_host: ioutil.ReadFile(%s) error %v\n", file, err)
		return ""
	}
	return chop(string(data))
}

func maybe_write_host(backup_root, be string, host string) {
	xhost := read_host(backup_root, be)
	if xhost != host {
		err := ioutil.WriteFile(host_file(backup_root, be), []byte(host+"\n"), 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ioutil.WriteFile() error %v\n", err)
		}
	}
}

func maybe_write_path(backup_root, be string, path string) {
	xpath := read_path(backup_root, be)
	if xpath != path {
		err := ioutil.WriteFile(path_file(backup_root, be), []byte(path+"\n"), 0700)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ioutil.WriteFile() error %v\n", err)
		}
	}
}

//
// get a list of all backup elements
//

func list_backup_elements(backup_root string) []string {
	//dir_glob := backup_root + "/*"
	//glob, err := filepath.Glob(dir_glob)
	dir, err := ioutil.ReadDir(backup_root)
	if err != nil {
		log.Fatalf("Cannot list backup elements, error %v\n", err)
	}

	var be_list []string

	for _, f := range dir {
		be_list = append(be_list, f.Name())
	}

	return be_list
}

func get_age(backup_root, be string) float64 {
	return file_age(lastlog_file(backup_root, be))
}

func get_lastreport(backup_root, be string) string {
	file := lastreport_file(backup_root, be)
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("get_lastlog: ioutil.ReadFile(%s) error %v\n", file, err)
		return ""
	}
	return chop(string(data))
}

//
// utility functions
//

func chop(s string) string {
	return strings.TrimSuffix(s, "\n")
}

func make_fstag(fs string) string {
	re := regexp.MustCompile("\\W")
	return re.ReplaceAllString(fs, "_")
}

func read_pipe(path string, file *os.File) string {
	s := ""
	buf := make([]byte, KiB)
	for {
		count, err := file.Read(buf)
		if err == io.EOF {
			return s
		}
		if err != nil {
			log.Printf("file.Read(%s) error %v\n", path, err)
			return s
		}
		s += string(buf[:count])
	}
}

func read_program(program string, args ...string) string {
	if *verbose {
		log.Printf("Reading exec %s\n", program)
	}
	// there is no exec.Command() that takes an array of args
	c := exec.Command(program)
	// manually append the command arguments
	a := make([]string, 1+len(args))
	a[0] = c.Args[0]
	copy(a[1:], args)
	c.Args = a
	// run the program, capture the output
	buf, err := c.CombinedOutput()
	if err != nil {
		log.Printf("os.exec.Command(%s).CombinedOutput() error %v\n", program, err)
		//return nil;
	}
	return chop(string(buf))
}

func file_age(file string) float64 {
	stat, err := os.Stat(file)
	if err != nil {
		return 0
	}
	if os.IsNotExist(err) {
		return 0
	}
	return time.Now().Sub(stat.ModTime()).Seconds()
}

func file_exists(file string) bool {
	_, err := os.Stat(file)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	log.Printf("file_exists(%s): os.Stat() error %v", file, err)
	return false
}

func read_config(file string) map[string]string {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("read_config: ioutil.ReadFile(%s) error %v\n", file, err)
		return nil
	}

	c := make(map[string]string)

	d1 := strings.Split((string)(data), "\n")

	split_re := regexp.MustCompile("(.*?)\\s*:\\s*(.*)")

	for k, v := range d1 {
		if strings.HasPrefix(v, "#") {
			continue
		}
		if strings.HasPrefix(v, " ") {
			continue
		}
		if strings.HasPrefix(v, "\t") {
			continue
		}
		if len(v) < 1 {
			continue
		}

		d2 := split_re.FindStringSubmatch(v)

		if len(d2) != 3 {
			log.Fatalf("read_config(%s): Line %d, Invalid entry %s\n", file, k+1, v)
			// DOES NOT RETURN
		}

		//fmt.Printf("d1 [%s], [%s] [%s] d2: %v\n", d1, d2[1], d2[2], d2);

		c[d2[1]] = d2[2]
	}

	return c
}

func MyParseUint(s string) uint64 {
	b := make([]byte, len(s))
	var bl int
	for i := 0; i < len(s); i++ {
		c := s[i]
		if c == ' ' {
			break
		}
		if c == ',' {
			continue
		}
		b[bl] = c
		bl++
	}
	ss := string(b[:bl])

	v, err := strconv.ParseUint(ss, 0, 64)
	if err != nil {
		log.Printf("MyParseUint: [%s] [%s] err [%v] %v\n", s, ss, err, v)
		return 0
	}
	return v
}

func generate_report(log_dir string) {

	if *verbose {
		log.Printf("Log dir: %s\n", log_dir)
	}

	logs, err := ioutil.ReadDir(log_dir)
	if err != nil {
		log.Fatalf("Cannot list log files, error %v\n", err)
	}

	report := ""

	for _, log_file := range logs {
		if !strings.HasSuffix(log_file.Name(), ".log") {
			continue
		}
		//fmt.Println(log_file.Name())

		report_line := log_to_report(log_file.Name())

		if *verbose {
			log.Printf("%s\n", report_line)
		}

		report += report_line
		report += "\n"
	}

	report_file := log_dir + "/report.txt"

	err = ioutil.WriteFile(report_file, []byte(report), 0444)

	if err != nil {
		log.Fatalf("Cannot write report file, error %v\n", err)
	}

	log.Printf("Report file: %s\n", report_file)
}

func generate_last_report(backup_root, be string) string {
	log_file := lastlog_file(backup_root, be)
	report_file := lastreport_file(backup_root, be)
	report := log_to_report(log_file)
	err := ioutil.WriteFile(report_file, []byte(report), 0444)
	if err != nil {
		log.Printf("Cannot write report file, error %v\n", err)
	}
	if *verbose {
		log.Printf("Report file: %s\n", report_file)
	}
	return report
}

func log_to_report(log_file string) string {
	data, err := ioutil.ReadFile(log_file)

	if err != nil {
		log.Printf("Cannot read log file, error %v\n", err)
		return ""
	}

	report_len := len(data)

	lines := bytes.Split(data, []byte("\n"))

	report_lines := len(lines)

	//
	// parse rsync log output:
	//
	// deleting home1/exo/.config/google-chrome/Default/IndexedDB/https_drive.google.com_0.indexeddb.blob/2/00/95
	// deleting home1/exo/.config/google-chrome/Default/IndexedDB/https_drive.google.com_0.indexeddb.blob/2/00/94
	//
	// Number of files: 6034046
	// Number of files transferred: 7017
	// Total file size: 795838698583 bytes
	// Total transferred file size: 1552222154 bytes
	// Literal data: 148831322 bytes
	// Matched data: 1403943368 bytes
	// File list size: 150596925
	// File list generation time: 361.588 seconds
	// File list transfer time: 0.000 seconds
	// Total bytes sent: 2348547
	// Total bytes received: 301056008
	//
	// And log output of /usr/bin/date
	//
	// 108.44user 836.94system 4:36:12elapsed 5%CPU (0avgtext+0avgdata 197812maxresident)k
	// 87142915inputs+12048529outputs (0major+52010minor)pagefaults 0swaps
	//
	//
	// rsync 3.1.2 has different log output:
	//
	// deleting home1/ucn_sequencer/.cache/google-chrome/Default/Cache/006e29f0a77a955e_0
	// deleting home1/ucn_sequencer/.cache/google-chrome/Default/Cache/
	// deleting home1/ucn_sequencer/.cache/google-chrome/Default/
	// deleting home1/ucn_sequencer/.cache/google-chrome/
	//
	// Number of files: 6,441,862 (reg: 5,813,747, dir: 582,226, link: 45,605, special: 284)
	// Number of created files: 671 (reg: 533, dir: 138)
	// Number of regular files transferred: 4,426
	// Total file size: 844,366,527,602 bytes
	// Total transferred file size: 4,277,616,697 bytes
	// Literal data: 130,288,053 bytes
	// Matched data: 4,148,028,570 bytes
	// File list size: 162,389,341
	// File list generation time: 500.574 seconds
	// File list transfer time: 0.000 seconds
	// Total bytes sent: 2,928,832
	// Total bytes received: 294,421,189
	//
	// sent 2,928,832 bytes  received 294,421,189 bytes  44,893.19 bytes/sec
	// total size is 844,366,527,602  speedup is 2,839.64
	// 156.09user 2032.88system 5:37:14elapsed 10%CPU (0avgtext+0avgdata 535336maxresident)k
	// 136101282inputs+8374153outputs (0major+142679minor)pagefaults 0swaps
	//
	// Error output:
	//
	// ssh: connect to host daq20 port 22: No route to host
	// rsync: connection unexpectedly closed (0 bytes received so far) [Receiver]
	// rsync error: unexplained error (code 255) at io.c(235) [Receiver=3.1.3]
	// Command exited with non-zero status 255
	// 0.01user 0.00system 0:03.08elapsed 0%CPU (0avgtext+0avgdata 4804maxresident)k
	// 93inputs+0outputs (0major+492minor)pagefaults 0swaps
	//

	var report_files uint64 = 0
	var report_files_xfer uint64 = 0
	var report_files_deleted uint64 = 0

	var report_size uint64 = 0
	var report_size_xfer uint64 = 0
	var report_size_literal uint64 = 0

	var report_list_time float64 = 0

	report_elapsed := ""

	for _, line := range lines {
		if bytes.HasPrefix(line, []byte("ssh: connect")) {
			return "Error: " + string(line)
		} else if bytes.HasPrefix(line, []byte("Host key verification failed")) {
			return "Error: " + string(line)
		} else if bytes.HasPrefix(line, []byte("Unable to negotiate with")) {
			return "Error: " + string(line)
		} else if bytes.HasPrefix(line, []byte("deleting ")) {
			report_files_deleted++
		} else if bytes.HasPrefix(line, []byte("Number of files: ")) {
			//report_files, err = strconv.ParseUint(strings.Trim(string(line[17:]), ","), 0, 64)
			//log.Printf("line [%s] [%s] err [%v] %v\n", line, line[17:], err, report_files);
			report_files = MyParseUint(string(line[17:]))
		} else if bytes.HasPrefix(line, []byte("Number of files transferred: ")) {
			report_files_xfer = MyParseUint(string(line[29:]))
			//log.Printf("line [%s] [%s] %v\n", line, line[29:], report_files_xfer);
		} else if bytes.HasPrefix(line, []byte("Number of regular files transferred: ")) {
			report_files_xfer = MyParseUint(string(line[37:]))
			//log.Printf("line [%s] [%s] %v\n", line, line[37:], report_files_xfer);
		} else if bytes.HasPrefix(line, []byte("Total file size: ")) {
			report_size = MyParseUint(string(line[17:]))
			//log.Printf("line [%s] [%s] %v\n", line, line[17:], report_size);
		} else if bytes.HasPrefix(line, []byte("Total transferred file size: ")) {
			report_size_xfer = MyParseUint(string(line[29:]))
			//log.Printf("line [%s] [%s] %v\n", line, line[29:], report_size_xfer);
		} else if bytes.HasPrefix(line, []byte("Literal data: ")) {
			report_size_literal = MyParseUint(string(line[14:]))
			//log.Printf("line [%s] [%s] %v\n", line, line[14:], report_size_literal);
		} else if bytes.HasPrefix(line, []byte("File list generation time: ")) {
			report_list_time, err = strconv.ParseFloat(strings.Trim(string(line[27:]), " seconds"), 64)
			//log.Printf("line [%s] [%s] %v\n", line, line[27:], report_list_time, err);
		} else if bytes.Index(line, []byte("elapsed")) > 0 {
			x := strings.Fields(string(line))
			if len(x) > 2 {
				report_elapsed = strings.TrimSuffix(x[2], "elapsed")
			}
		}
	}

	report_size_MiB := float64(report_size) / (1024.0 * 1024.0)
	report_size_xfer_MiB := float64(report_size_xfer) / (1024.0 * 1024.0)
	report_size_literal_MiB := float64(report_size_literal) / (1024.0 * 1024.0)

	report_line := fmt.Sprintf("%10d %8d %8d %6d %6d %10.1f %8.1f %8.1f %7.1f %s",
		report_len,
		report_lines,
		report_files,
		report_files_xfer,
		report_files_deleted,
		report_size_MiB,
		report_size_xfer_MiB,
		report_size_literal_MiB,
		report_list_time,
		report_elapsed)

	return report_line
}

func cleanup(backup_root, zfs_root, be string) {
	newlog_file := newlog_file(backup_root, be)
	lastlog_file := lastlog_file(backup_root, be)

	_, err := os.Stat(newlog_file)
	if err != nil {
		// no newlog_file, return
		if *verbose {
			log.Printf("Cleanup: %s - no new log file, no need to cleanup\n", be)
		}
		return
	}

	if *verbose {
		log.Printf("Cleanup: %s\n", be)
	}

	// rename log file

	err = os.Rename(newlog_file, lastlog_file)
	if err != nil {
		log.Printf("Rename log file error %v\n", err)
	}

	// generate report

	generate_last_report(backup_root, be)

	// create ZFS snapshot

	zfs_target_path := zfs_root + "/" + be

	t := time.Now()

	ts := t.Format("20060102-15:04:05")

	snapshot_name := ts + "-cleanup"

	if *verbose {
		log.Printf("Snapshot: %s\n", snapshot_name)
	}

	zfs_snapshot_command := "zfs snapshot " + zfs_target_path + "@" + snapshot_name

	if *verbose {
		log.Printf("ZFS snapshot command: %s\n", zfs_snapshot_command)
	}

	c := exec.Command("/bin/sh", "-c", zfs_snapshot_command)
	sout, err := c.CombinedOutput()
	if err != nil {
		log.Printf("ZFS snapshot failed, error %v\n", err)
		log.Printf("ZFS snapshot output: %s\n", chop(string(sout)))
	}
}

func backup(backup_root, zfs_root, be string) string {

	if *dryrun {
		data, err := ioutil.ReadFile(lastreport_file(backup_root, be))
		if err != nil {
			return fmt.Sprintf("Error: Cannot read log file, error %v\n", err)
		}
		return string(data)
	}

	cleanup(backup_root, zfs_root, be)

	host := read_host(backup_root, be)
	path := read_path(backup_root, be)

	if len(host) < 1 {
		return "Error: no host"
	}

	if len(path) < 1 {
		return "Error: no path"
	}

	if *verbose {
		log.Printf("Backup of %s from %s:%s\n", be, host, path)
	}

	log_file := newlog_file(backup_root, be)
	exclude_file := exclude_file(backup_root, be)

	t := time.Now()

	ts := t.Format("20060102-15:04:05")

	//
	// rsync maybe add these flags:
	// -W - just transfer the files, do not read them to try to minimize transfer bytes
	// --ignore-errors - delete files even if there were errors
	// -i - for debugging - show all activities and why they were done

	rsync_command := "/usr/bin/time /usr/bin/rsync -avx --delete-during --delete-excluded --ignore-errors --stats --exclude-from=" + exclude_file + " " + host + ":" + path + " " + backup_root + "/" + be + "/data >> " + log_file + " 2>&1"

	if *verbose {
		log.Printf("Rsync command: %s\n", rsync_command)
	}

	c := exec.Command("/bin/sh", "-c", rsync_command)

	var out bytes.Buffer
	c.Stdout = &out
	c.Stderr = &out

	err := c.Start()
	if err != nil {
		log.Printf("Rsync start failed, error %v\n", err)
		fmt.Fprintf(os.Stderr, "Rsync output: %s\n", chop(string(out.Bytes())))
	} else {
		err = c.Wait()
		if err != nil {
			log.Printf("Rsync finished with error %v\n", err)
			fmt.Fprintf(os.Stderr, "Rsync output: %s\n", chop(string(out.Bytes())))
		}
	}

	//  rename log file

	lastlog_file := lastlog_file(backup_root, be)

	if *verbose {
		log.Printf("Log file: %s\n", lastlog_file)
	}

	err = os.Rename(log_file, lastlog_file)
	if err != nil {
		log.Printf("Rename log file error %v\n", err)
	}

	report := generate_last_report(backup_root, be)

	// create ZFS snapshot

	snapshot_name := ts

	if *verbose {
		log.Printf("Snapshot: %s\n", snapshot_name)
	}

	zfs_target_path := zfs_root + "/" + be

	zfs_snapshot_command := "zfs snapshot " + zfs_target_path + "@" + snapshot_name

	if *verbose {
		log.Printf("ZFS snapshot command: %s\n", zfs_snapshot_command)
	}

	c = exec.Command("/bin/sh", "-c", zfs_snapshot_command)
	sout, err := c.CombinedOutput()
	if err != nil {
		log.Printf("ZFS snapshot failed, error %v\n", err)
		log.Printf("ZFS snapshot output: %s\n", chop(string(sout)))
	}

	return report
}

func report_all(backup_root, report_root, report_rsync string) {

	be_list := list_backup_elements(backup_root)

	all_reports := ""

	for _, be := range be_list {
		host := read_host(backup_root, be)
		if len(host) < 1 {
			continue
		}
		path := read_path(backup_root, be)
		if len(path) < 1 {
			continue
		}
		report := generate_last_report(backup_root, be)

		all_reports += fmt.Sprintf("%-30s %s\n", host+":"+path, report)
		//all_reports += host;
		//all_reports += ":";
		//all_reports += path;
		//all_reports += " ";
		//all_reports += report;
		//all_reports += "\n";

	}

	last_all_reports_file := report_root + "/last_report.txt"

	err := ioutil.WriteFile(last_all_reports_file, []byte(all_reports), 0444)
	if err != nil {
		log.Printf("Cannot write report file, error %v\n", err)
	}

	log.Printf("Report file: %s\n", last_all_reports_file)

	//log_dir := backup_root + "/*/logs"
	//
	//if *verbose {
	//	log.Printf("Log dir: %s\n", log_dir)
	//}
	//
	//logs, err := filepath.Glob(log_dir)
	//if err != nil {
	//	log.Fatalf("Cannot list log files, error %v\n", err)
	//}
	//
	//for _, log_file := range logs {
	//	if *verbose {
	//		fmt.Println(log_file)
	//	}
	//
	//	generate_report(log_file)
	//}

	if len(report_rsync) > 0 {
		copy_all(report_root, report_rsync)
	}
}

func copy_all(report_root, report_rsync string) {
	rsync_command := "rsync -av "
	rsync_command += report_root
	rsync_command += "/"
	rsync_command += " "
	rsync_command += report_rsync

	log.Printf("Copy results: %s\n", rsync_command)

	c := exec.Command("/bin/sh", "-c", rsync_command)
	sout, err := c.CombinedOutput()
	if err != nil {
		log.Printf("rsync command failed, error %v\n", err)
		log.Printf("rsync command output: %s\n", chop(string(sout)))
	}
}

func send_email(to, subject, msg string) {
	msg = "Subject: " + subject + "\r\n" + msg

	log.Printf("Sending email to %s with subject %s\n", to, subject)

	if *verbose {
		fmt.Println("Email to: " + to)
		fmt.Println("Email subject: " + subject)
		fmt.Println("Email text:")
		fmt.Println(msg)
	}
	//xto := []string{ to }
	//err := smtp.SendMail("localhost:25", nil, "gobackup@localhost", xto, []byte(msg))
	//if err != nil {
	//	log.Printf("Cannot send email: %v\n", err)
	//}

	// Connect to the remote SMTP server.
	c, err := smtp.Dial("localhost:25")
	if err != nil {
		log.Printf("Cannot send email, smtp.Dial() error %v\n", err)
		return
	}

	// Set the sender
	if err := c.Mail("root@localhost"); err != nil {
		log.Printf("Cannot send email, smtp.Mail() error %v\n", err)
		return
	}

	// Set recipient
	if err := c.Rcpt(to); err != nil {
		log.Printf("Cannot send email, smtp.Rcpt() error %v\n", err)
		return
	}

	// Send the email body.
	wc, err := c.Data()
	if err != nil {
		log.Printf("Cannot send email, smtp.Data() error %v\n", err)
		return
	}

	_, err = fmt.Fprintf(wc, msg)
	if err != nil {
		log.Printf("Cannot send email, Fprintf() error %v\n", err)
		return
	}

	err = wc.Close()
	if err != nil {
		log.Printf("Cannot send email, smtp.Close() error %v\n", err)
		return
	}

	// Send the QUIT command and close the connection.
	err = c.Quit()
	if err != nil {
		log.Printf("Cannot send email, smtp.Quit() error %v\n", err)
		return
	}
}

func delete(zfs_root string) string {

	log.Printf("Deleting old backups from ZFS pool \"%s\"\n", zfs_root)

	zfs_list_root := read_program("zfs", "list", "-p", "-H", zfs_root)

	e := strings.Fields(zfs_list_root)
	if e[0] != zfs_root {
		return fmt.Sprintf("Error: Cannot get free space of ZFS pool \"%s\"\n", zfs_root)
	}

	available, _ := strconv.ParseFloat(e[2], 64)

	log.Printf("ZFS pool \"%s\" has %s free space\n", zfs_root, toGiB(available))

	zfs_list := read_program("zfs", "list", "-H", "-p", "-t", "all", "-r", zfs_root) // | grep ^pool/gobackup`;

	xfs := make(map[string]string)
	xdate := make(map[string]string)
	xused := make(map[string]float64)
	xrefer := make(map[string]float64)

	all_dates_map := make(map[string][]string)

	for _, e := range strings.Split(zfs_list, "\n") {
		//fmt.Printf("BBB [%s]\n", e);
		ee := strings.Fields(e)
		name := ee[0]
		if strings.HasPrefix(name, zfs_root) &&
			strings.Contains(name, "@") {
			if len(ee) < 5 {
				continue
			}

			used, _ := strconv.ParseFloat(ee[1], 64)
			//avail := ee[2]
			refer, _ := strconv.ParseFloat(ee[3], 64)
			//mountpoint := ee[4]

			nn := strings.Split(name, "@")

			if len(nn) != 2 {
				continue
			}

			fs := nn[0]
			date := nn[1]

			//fmt.Printf("[%s] [%s] [%s] [%s] [%s] [%s] [%s]\n", name, used, avail, refer, mountpoint, fs, date)

			xfs[name] = fs
			xdate[name] = date
			xused[name] = used
			xrefer[name] = refer
			all_dates_map[date] = append(all_dates_map[date], name)
		}
	}

	fs_used := make(map[string]float64)
	fs_max_used := make(map[string]float64)

	total_used := 0.0

	//foreach my $k (sort {$used{$b} <=> $used{$a}} keys %date)
	for k, _ := range xdate {
		//name := k;
		//date := xdate[k]
		used := xused[k]
		//refer := xrefer[k];
		fs := xfs[k]

		total_used += used
		fs_used[fs] += used

		if used > fs_max_used[fs] {
			fs_max_used[fs] = used
		}
	}

	log.Printf("Used by snapshots: %s\n", toGiB(total_used))

	fs_max_used_sum := 0.0

	for fs, _ := range fs_used {
		//fs_used := fs_used[fs]
		fs_max_used := fs_max_used[fs]
		fs_max_used_sum += fs_max_used
	}

	mult := 2.0
	wanted_free_space := mult * fs_max_used_sum
	to_delete := wanted_free_space - available

	log.Printf("Wanted free space: %s, available: %s, need to delete: %s\n",
		toGiB(wanted_free_space),
		toGiB(available),
		toGiB(to_delete))

	var all_dates []string

	for k, _ := range all_dates_map {
		all_dates = append(all_dates, k)
	}

	// sort in increasing order,
	// is same as sort by date,
	// early first, late next.

	sort.Strings(all_dates)

	cumul_used := 0.0
	count_deleted := 0

	// loop over all dates, starting from earliest
	// delete snapshots, biggest first

	for _, date := range all_dates {
		names := all_dates_map[date]
		//fmt.Printf("date %s: %v\n", date, names)

		// sort names by size
		sort.Slice(names, func(i, j int) bool { return xused[names[i]] >= xused[names[j]] })

		for _, name := range names {
			used := xused[name]
			//fmt.Printf("date %s: %s uses %s\n", date, name, toGiB(used))

			if cumul_used > to_delete {
				break
			}

			cumul_used += used

			if *dryrun {
				fmt.Printf("Will delete %s size %s, cumulative %s, need %s more\n", name, toGiB(used), toGiB(cumul_used), toGiB(to_delete-cumul_used))
			} else {
				log.Printf("Delete ZFS snapshot %s\n", name)
				count_deleted++
				read_program("zfs", "destroy", name)
			}
		}

		if cumul_used > to_delete {
			break
		}
	}

	zfs_list_root = read_program("zfs", "list", "-p", "-H", zfs_root)

	e = strings.Fields(zfs_list_root)
	if e[0] != zfs_root {
		return fmt.Sprintf("Error: Cannot get free space of ZFS pool \"%s\"", zfs_root)
	}

	done_available, _ := strconv.ParseFloat(e[2], 64)

	report := fmt.Sprintf("ZFS pool \"%s\" free space: %s, before delete of %d snapshots using %s: %s", zfs_root, toGiB(done_available), count_deleted, toGiB(cumul_used), toGiB(available))

	log.Printf("%s\n", report)

	return report
}

func toGiB(sz float64) string {
	return fmt.Sprintf("%.3f GiB", sz/(1024.0*1024.0*1024.0))
}

// end
